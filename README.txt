-- SUMMARY --

The MediaWiki Users module allows using an existing MediaWiki users table to control user login and management.
In its current version, the module take over entirely of the user login form, so users registered via Drupal will not be allowed to log in (one exception is user #1).


-- REQUIREMENTS --

A MediaWiki users table. Currently tested with MediaWiki 1.12.


-- INSTALLATION --

* Install as usual.

* Enable via admin/modules.

-- CONFIGURATION --

Open settings.php and add the following configuration:

/**
 * The name of the table where MediaWiki users are managed.
 * If it's in a different DB, then set to dbname.tablename
 * Remember to add the proper prefix entry in $databases.
 */
$conf['mediawiki_users_table'] = 'mediawiki_user';

/**
 * The user name of user #1.
 * This is required to keep user #1 super admin permissions.
 */
$conf['mediawiki_users_admin'] = 'administrator';


-- KNOWN ISSUES --

* The module will break if there are existing users in the Drupal users table with the same user name as a MediaWiki user.
  

-- CONTACT --

Current maintainer:
Alon Pe'er (alonpeer) - http://drupal.org/user/47135

This project has been sponsored by SupersonicAds (http://www.supersonicads.com).
